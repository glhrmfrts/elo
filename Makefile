SRC = $(wildcard src/*.cpp)
OBJ = ${SRC:.cpp=.o}

CC = clang
CFLAGS = -g -O0 -std=c++14 -Wall -Wextra -Wno-unused-parameter

LDFLAGS += -fno-use-linker-plugin -lm

# output

OUT = elo
ifdef SystemRoot
	OUT = elo.exe
endif

$(OUT): $(OBJ)
	$(CC) $^ $(LDFLAGS) -o $@

%.o: %.cpp
	@$(CC) -c $(CFLAGS) $< -o $@
	@printf "\e[36mCC\e[90m %s\e[0m\n" $@

clean:
	rm -f $(OUT) $(OBJ)

.PHONY: clean
